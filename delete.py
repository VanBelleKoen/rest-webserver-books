import mongo_action as ma

def delete_id(request):
    _id = request.args['id']

    return ma.delete_one(_id)

def delete_all():
    return ma.delete_all()    