from io import UnsupportedOperation
from bson.objectid import ObjectId
import flask
from flask import request, jsonify
from werkzeug.exceptions import BadRequest, BadRequestKeyError, MethodNotAllowed
import get as g
import delete as d
import post as p

app = flask.Flask(__name__)

@app.route('/ping', methods=['GET'])
def home():
    return 'Success'

@app.route('/api/v1/resources/books/all', methods=['GET', 'DELETE'])
def api_all():
    if request.method == 'GET':
        return g.get_all()
    elif request.method == 'DELETE':
        return d.delete_all()
    else:
        raise MethodNotAllowed

@app.route('/api/v1/resources/books', methods=['GET', 'DELETE'])
def api_id():
    if ObjectId.is_valid(request.args['id']):
        if request.method == 'GET':
            return g.get_id(request) 

        elif request.method == 'DELETE':
            return d.delete_id(request)
        else:
            raise MethodNotAllowed  
    else:
        raise BadRequest

@app.route('/api/v1/resources/books', methods=['POST'])
def api_add():
    return p.post_one(request)

@app.errorhandler(BadRequest)
def handle_bad_request(e):
    return 'Bad request', 400

@app.errorhandler(MethodNotAllowed)
def handle_bad_request(e):
    return 'Bad request', 405    

@app.errorhandler(BadRequestKeyError) 
def handle_missing_key(e):
    return jsonify({'Missing key(s)':  e.args}), 400       
    
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')