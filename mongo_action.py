from bson import ObjectId

import common as c
import database as d


def insert(data):
    x = d.collection.insert_one(data)
    return x.inserted_id

def find_id(_id):
    mydoc = d.collection.find_one({ "_id" : ObjectId(_id) })
    return c.jsonify_dicts(mydoc)

def find_all():
    docs = list(d.collection.find({}))
    return c.jsonify_lists(docs)

def delete_one(_id):
    d.collection.delete_one({ "_id" : ObjectId(_id) })
    return "Success"

def delete_all():
    d.collection.delete_many({})
    return "Success"