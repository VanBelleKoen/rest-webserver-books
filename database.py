import pymongo
from pymongo import collection

myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["mydatabase"]
collection = mydb["books"]

if "mydatabase" in myclient.list_database_names():
    print("The database already exists.")
else:
    print("Creating the database")