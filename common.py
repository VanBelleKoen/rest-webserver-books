import json
from bson import json_util

def jsonify_dicts(data):

    return json.loads(json_util.dumps((data)))
