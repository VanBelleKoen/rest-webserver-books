from flask.json import jsonify
import mongo_action as ma


def get_id(request):
    _id = request.args['id']
    mydoc = ma.find_id(_id)

    if mydoc == None:
        return "Id not found", 404
    
    return mydoc, 200

def get_all():
    mydocs = ma.find_all()

    return jsonify(mydocs), 200
