# Rest-Server

The project will focus on enabling the education of REST calls. As a small webservice program, this should enable the user to learn how to test and validate the different types of rest calls.

## Rest calls

The REST calls will interact with a mongo database service. At the moment the training-wheels program supports GET, POST, DELETE. There are two different versions of GET and the DELETE. One in which all books are returned and one in which a specific book will be given based on the id parameter in the call itself.

### GET

- /ping
- /api/v1/resources/books/all
- /api/v1/resources/books?id=?

### DELETE

- /api/v1/resources/books/all
- /api/v1/resources/books?id=?

### POST

- /api/v1/resources/books

```json
    {
        "title": string,
        "author": string,
        "first_sentence": string,
        "published": string
    }
```

The response will contain the full json-object including the id which was used to store the new book. This is also the id required in order to GET and DELETE the different books inside the database file.

## The Application

This application should only be used in order to test rest calls. The internal database, which is a huge overstatement, is a json file that contains three different books. Any other type of use has not been tested or validated.

## Docker Use

> Docker is no longer working due to the addition of a mongo database interaction

In order to use the docker functionality of this python application the following two commands are needed. In the command-line navigate towards to directory where you've stored this application. More importantly this directory should have the Dockerfile which in turn will be used by the commands in order to create the docker image itself and run that docker image inside a container. After which postman can be used in order to test the rest-service.

I suggest using the ping call as your first test call.

```bash
docker build -t webserver .
```

```bash
docker run -d -p 5000:5000 webserver
```

And now you can fire the requests to localhost:5000
